# Copy and paste

Unix-like systems running a graphical display through Xorg,
when a text is selected with a mouse goes to `primary` selection,
when pressed <kbd>Ctrl</kbd> + <kbd>c</kbd> or
<kbd>Ctrl</kbd> + <kbd>x</kbd> goes to `clipboard` selection

The `xclip` supports selections "primary", "secondary", "clipboard" or "buffer-cut"

In the examples, just replace `clipboard` with the selection you want

Install

```
# apt install -y xclip
```

## Copy

Copy file contents for `clipboard` selection

```
$ xclip -in -selection clipboard file.txt
```

Copy console output for `clipboard` selection

```
$ ls | xclip -in -selection clipboard
```

## Paste

Paste in file to `clipboard` selection

```
$ xclip -out -selection clipboard > file.txt
```

Paste in console to `clipboard` selection

```
$ xclip -out -selection clipboard
```
